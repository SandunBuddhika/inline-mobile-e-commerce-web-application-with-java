package com.sandun.web.controllers;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import org.glassfish.jersey.server.mvc.Viewable;

@Path("/home")
public class HomeController {
    @GET
    public String doGet(){
        return "Hello, World!";
    }
}
