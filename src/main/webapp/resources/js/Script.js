let allOuterDiv = document.querySelectorAll(".outer_div");

allOuterDiv.forEach((scrollDiv, index)=>{
    scrollDiv.addEventListener('scroll', () => toggleScrollButtons((index+1)));
});

function scroller(leftOrRight, index) {
    let scrollDiv = document.getElementById("outer_div" + index);
    if (leftOrRight == 1) {
        scrollDiv.scrollBy(scrollDiv.offsetWidth - 250, 0);
    } else {
        scrollDiv.scrollBy(-scrollDiv.offsetWidth + 250, 0);
    }
}

function toggleScrollButtons(index) {
    let scrollDiv = document.getElementById("outer_div" + index);
    let rightScrollBtn = document.getElementById("right_scroll_btn"+index);
    let leftScrollBtn = document.getElementById("left_scroll_btn"+index);
    const scrollLeft = scrollDiv.scrollLeft;
    const scrollWidth = scrollDiv.scrollWidth;
    const clientWidth = scrollDiv.clientWidth;

    if (scrollLeft > 0) {
        leftScrollBtn.classList.remove('d-none');
    } else {
        leftScrollBtn.classList.add('d-none');
    }

    if (scrollLeft + clientWidth >= scrollWidth) {
        rightScrollBtn.classList.add('d-none');
    } else {
        rightScrollBtn.classList.remove('d-none');
    }
}

